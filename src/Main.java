import Clases.Menu;
import Clases.DB.VehiculosDB;

import java.util.Scanner;

/**
 * Proyecto para una concesionaria de autos e importación de motores que debe cumplir con las siguientes
 * funcionalidades:
 * Mostrar una lista de los vehículos que se encuentran en stock.
 * Eliminar un vehículo de la lista por medio de su id.
 * Probar los motores que se encuentran en stock.
 * Recargar el combustible de los automóviles que se usaron para probar los motores.
 * Cambiar la promoción del mes.
 * Ver la promoción vigente actualmente.
 */
public class Main
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu(new VehiculosDB());
        int input; // opción del menu

        do {
            menu.menuTabla();
            input = scanner.nextInt();
            menu.menuOpciones(input);
        } while (input != 0);
    }
}


