package Clases;

import Clases.Services.Motores;
import Clases.Services.Promociones;
import Interfaces.IVehiculosDB;

/**
 * Clase que contiene los servicios y los vehículos disponibles. Muestra y ejecuta las diferentes
 * funcionalidades que tiene cada uno de los servicios y la lista de vehículos.
 */
public class Menu
{

    private IVehiculosDB vehiculos;
    private Motores motores;
    private Promociones promociones;

    public Menu(IVehiculosDB vehiculos) {
        this.vehiculos = vehiculos;
        this.promociones = new Promociones();
        this.motores = new Motores();
    }

    /**
     * Muestra por terminal las funcionalidades del sistema para seleccionar una de ellas de acuerdo
     * al número que se ingresa.
     */
    public void menuTabla() {
        System.out.println("----------OPCIONES---------");
        System.out.println("1.- Mostrar vehiculos");
        System.out.println("2.- Eliminar vehiculo por id");
        System.out.println("3.- Probar motores en stock");
        System.out.println("4.- Recargar combustible");
        System.out.println("5.- Cambiar promocion");
        System.out.println("6.- Ver promocion actual");
        System.out.println("0 salir del sistema");
        System.out.println("Ingrese una opción: ");
    }

    /**
     * ejecuta una de las funcionalidades de acuerdo al número pasado por parámetro.
     * @param opcion número que indica la funcionalidad a ejecutar.
     */
    public void menuOpciones(int opcion) {
        switch (opcion) {
            case 1 -> this.vehiculos.mostrarVehiculos();
            case 2 -> System.out.println(this.vehiculos.eliminarPorId());
            case 3 -> this.motores.probarMotores();
            case 4 -> this.motores.recargarMotores();
            case 5 -> this.promociones.seleccionarPromo();
            case 6 -> System.out.println(this.promociones.verPromoValida());
            case 0 -> System.out.println("Hasta Pronto!!!");
        }
    }

}