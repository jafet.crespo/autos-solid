package Clases.DB;

import Clases.Vehiculos.AutoLujo;
import Clases.Vehiculos.Camioneta;
import Clases.Vehiculos.Motocicleta;
import Clases.Vehiculos.Vehiculo;
import Interfaces.IVehiculosDB;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase que implementa la interfaz IVehiculosDB. Permite almacenar y utilizar las funcionalidades de los vehículos
 * almacenados en un variable ArrayList de tipo Vehiculo.
 */
public class VehiculosDB implements IVehiculosDB
{

    private final ArrayList<Vehiculo> listaVehiculos;

    public VehiculosDB() {
        this.listaVehiculos = new ArrayList<>();
        this.listaVehiculos.add(new Motocicleta(1, "moto 1", "rojo", "780MM"));
        this.listaVehiculos.add(new Motocicleta(2, "moto 2", "azul", "780MM"));
        this.listaVehiculos.add(new AutoLujo(3, "lujo 1", "amarrillo", "22 millones"));
        this.listaVehiculos.add(new AutoLujo(4, "lujo 1", "verde", "15 millones"));
        this.listaVehiculos.add(new Camioneta(5, "camioneta 1", "rojo", "2 TON"));
        this.listaVehiculos.add(new Camioneta(6, "camioneta 2", "verde", "1.5 TON"));
    }


    @Override
    public void mostrarVehiculos() {
        listaVehiculos.forEach(vehiculo -> System.out.println(vehiculo.toString()));
    }

    @Override
    public String eliminarPorId() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ingrese el id de un vehiculo:");
        int input = scanner.nextInt(); // Valor del entero ingresado que representa el Id del vehículo
        return this.listaVehiculos.removeIf(vehiculo -> vehiculo.getId() == input)
                ? "vehiculo eliminado!!"
                : "Id incorrecto";
    }

}