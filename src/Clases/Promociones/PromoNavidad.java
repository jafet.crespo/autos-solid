package Clases.Promociones;

import Interfaces.IPromocion;

import java.time.LocalDate;

/**
 * Clase que implementa la interfaz IPromocion. Crea una clase con los atributos de nombre, descripcion, startDate y
 * finishDate relacionados con la celebración de navidad. Se debe pasar como parámetro el año que se aplicara
 * la promoción y la cantidad de descuento.
 */
public class PromoNavidad implements IPromocion
{

    private String nombre;
    private int descuento;
    private String descripcion;
    private LocalDate startDate;
    private LocalDate finishDate;

    public PromoNavidad(int descuento, int year) {
        this.descuento = descuento;
        this.nombre = "NAVIDAD";
        this.descripcion = "Promoción por fechas navideñas, traer un juguete para aplicar el descuento";
        this.startDate = LocalDate.of(year, 12, 1);
        this.finishDate = LocalDate.of(year, 12, 31);
    }

    public String getNombre() {
        return this.nombre;
    }

    @Override
    public int getDescuento() {
        return this.descuento;
    }

    @Override
    public String getDescripcion() {
        return this.descripcion;
    }

    @Override
    public LocalDate getStartDate() {
        return this.startDate;
    }

    @Override
    public LocalDate getFinishDate() {
        return this.finishDate;
    }

}