package Clases.Motores;

import Interfaces.IElectricEngine;

/**
 * Clase para crear una instancia de un motor Tesla eléctrico. Implementa la interfaz IElectricEngine para
 * motores eléctricos.
 */
public class TeslaEngine implements IElectricEngine
{

    @Override
    public void chargeBattery() {
        System.out.println("CARGANDO BATERIA.......");
    }

    @Override
    public void baterryLife() {
        System.out.println("tiempo de vida 10 años");
    }

    @Override
    public void iniciar() {
        System.out.println("INICIANDO motor electrico Tesla........");
    }

    @Override
    public void apagar() {
        System.out.println("APAGANDO motor electrico Tesla...... ");
    }

    @Override
    public void acelerar() {
        System.out.println("ACELERANDO motor electrico Tesla...... ");
    }

    @Override
    public void frenar() {
        System.out.println("FRENANDO motor electrico Tesla...... ");
    }

    @Override
    public void refuel() {
        chargeBattery();
        baterryLife();
    }

}