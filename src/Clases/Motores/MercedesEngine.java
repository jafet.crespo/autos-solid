package Clases.Motores;

import Interfaces.IElectricEngine;
import Interfaces.IGasEngine;

/**
 * Clase para crear una instancia de un motor Mercedes hibrido (motor a combustión a gasolina y motor eléctrico).
 * Implementa la interfaz IGasEngine y IElectricEngine por ser un motor híbrido que cuenta con las funcionalidades
 * de ambos tipos de motor.
 */
public class MercedesEngine implements IElectricEngine, IGasEngine
{

    @Override
    public void chargeBattery() {
        System.out.println("CARGANDO BATERIA DEL AUTO HIBRIDO ........");
    }

    @Override
    public void baterryLife() {
        System.out.println("Tiempo de vida de 15 años");
    }

    @Override
    public void iniciar() {
        System.out.println("iniciando motor hibrido de mercedes........");
    }

    @Override
    public void apagar() {
        System.out.println("apagando motor hibrido de mercedes........");
    }

    @Override
    public void acelerar() {
        System.out.println("acelerando motor hibrido de mercedes.........");
    }

    @Override
    public void frenar() {
        System.out.println("acelerando motor hibrido de mercedes.........");
    }

    @Override
    public void refuel() {
        chargeBattery();
        baterryLife();
        fillGasTank();
        maxSize();
    }

    @Override
    public void fillGasTank() {
        System.out.println("llenando tanque de gasolina de motor hibrido.......");
    }

    @Override
    public void maxSize() {
        System.out.println("maximo 16 galones");
    }

}