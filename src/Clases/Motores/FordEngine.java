package Clases.Motores;

import Interfaces.IGasEngine;

/**
 * Clase para crear una instancia de un motor Ford a gasolina. Implementa la interfaz IGasEngine para
 * motores a gasolina.
 */
public class FordEngine implements IGasEngine
{

    @Override
    public void iniciar() {
        System.out.println("INICIANDO motor a gasolina Ford........");
    }

    @Override
    public void apagar() {
        System.out.println("APAGANDO motor a gasolina Ford...... ");
    }

    @Override
    public void acelerar() {
        System.out.println("ACELERANDO motor a gasolina Ford...... ");
    }

    @Override
    public void frenar() {
        System.out.println("FRENANDO motor a gasolina Ford...... ");
    }

    @Override
    public void refuel() {
        fillGasTank();
        maxSize();
    }

    @Override
    public void fillGasTank() {
        System.out.println("Llenando Tanque de gasolina...... ");
    }

    @Override
    public void maxSize() {
        System.out.println("Maximo 17 galones ");
    }

}