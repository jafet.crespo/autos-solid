package Clases.Vehiculos;

/**
 * Clase abstracta de vehículo, que contiene los atributos básicos de cada vehículo y pueden
 * ser mostrados por pantalla.
 */
public abstract class Vehiculo
{

    private int id;
    private String marca;
    private String color;

    public Vehiculo(int id, String marca, String color) {
        this.id = id;
        this.marca = marca;
        this.color = color;
    }

    /**
     * devuelve el Id del vehículo para buscarlo en una lista de vehículos.
     * @return regresa un número entero, el cual es el Id del vehículo.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Texto con los atributos y características especiales de cada vehículo.
     * Se muestra en formato JSON.
     * @return una cadena de texto con formato JSON de las características del vehículo.
     */
    @Override
    public String toString() {
        return "Vehiculo{" +
                "id=" + this.id +
                ", marca='" + this.marca + '\'' +
                ", color='" + this.color + '\'' +
                ", caracteristicas= " + caracteristicaEspecial() +
                '}';
    }

    /**
     * Función abstracta, que devuelve las características especiales que tiene cada hijo de la
     * clase vehículo.
     * @return cadena de texto en formato JSON, con las características especiales de cada vehículo
     */
    public abstract String caracteristicaEspecial();
}