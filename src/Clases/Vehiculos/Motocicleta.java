package Clases.Vehiculos;

/**
 * Clase que extiende de Vehículo, cuenta con un atributo tipo con el valor de "motocicleta" y un
 * atributo seatHeight(altura del asiento) pasado como parámetro en el constructor.
 */
public class Motocicleta extends Vehiculo
{

    private String tipo;
    private String seatHeight;

    public Motocicleta(int id, String marca, String color, String seatHeight) {
        super(id, marca, color);
        this.tipo = "motocicleta";
        this.seatHeight = seatHeight;
    }

    @Override
    public String caracteristicaEspecial() {
        return "{" +
                "tipo='" + this.tipo + '\'' +
                ", seatHeight='" + this.seatHeight + '\'' +
                '}';
    }

}