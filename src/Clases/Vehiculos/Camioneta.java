package Clases.Vehiculos;

/**
 * Clase que extiende de Vehículo, cuenta con un atributo tipo con el valor de "camioneta" y un
 * atributo cargaMaxima pasado como parámetro en el constructor.
 */
public class Camioneta extends Vehiculo
{

    private String tipo;
    private String cargaMaxima;

    public Camioneta(int id, String marca, String color, String cargaMaxima) {
        super(id, marca, color);
        this.tipo = "camioneta";
        this.cargaMaxima = cargaMaxima;
    }

    @Override
    public String caracteristicaEspecial() {
        return "{" +
                "tipo='" + this.tipo + '\'' +
                ", cargaMaxima='" + this.cargaMaxima + '\'' +
                '}';
    }

}