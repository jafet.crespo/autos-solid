package Clases.Vehiculos;

/**
 * Clase que extiende de Vehículo, cuenta con un atributo tipo con el valor de "Auto de lujo" y un
 * atributo precio pasado como parámetro en el constructor.
 */
public class AutoLujo extends Vehiculo
{

    private String tipo;
    private String precio;

    public AutoLujo(int id, String marca, String color, String precio) {
        super(id, marca, color);
        this.precio = precio;
        this.tipo = "Auto de lujo";
    }


    @Override
    public String caracteristicaEspecial() {
        return "{" +
                "tipo='" + this.tipo + '\'' +
                ", precio='" + this.precio + '\'' +
                '}';
    }

}