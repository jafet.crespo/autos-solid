package Clases.Services;

import Clases.Motores.FordEngine;
import Clases.Motores.MercedesEngine;
import Clases.Motores.TeslaEngine;
import Clases.Motores.ToyotaEngine;
import Interfaces.IEngine;

import java.util.ArrayList;

/**
 * Clase que permite el manejo de las funcionalidades de las clases motor. Se almacenan los motores en un
 * ArrayList de tipo IEngine.
 */
public class Motores
{
    private ArrayList<IEngine> listaMotores;

    public Motores() {
        listaMotores = new ArrayList<>();
        this.listaMotores.add(new FordEngine());
        this.listaMotores.add(new ToyotaEngine());
        this.listaMotores.add(new TeslaEngine());
        this.listaMotores.add(new MercedesEngine());
    }

    /**
     * Prueba el funcionamiento de todos los motores que se encuentran en la lista.
     * Se prueba que los motores enciendan, aceleren, frenen y se apaguen.
     */
    public void probarMotores() {
        this.listaMotores.forEach(motor -> {
            motor.iniciar();
            motor.acelerar();
            motor.frenar();
            motor.apagar();
            System.out.println("---------------------------------------");
        });
    }

    /**
     * Recarga el combustible de todos los motores que se encuentran en la lista.
     */
    public void recargarMotores() {
        this.listaMotores.forEach(motor -> {
            motor.refuel();
            System.out.println("---------------------------------------");
        });
    }

}