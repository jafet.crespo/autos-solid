package Clases.Services;

import Clases.Promociones.PromoCarnaval;
import Clases.Promociones.PromoHalloween;
import Clases.Promociones.PromoNavidad;
import Interfaces.IPromocion;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Clase que permite manejar las funcionalidades de las clases promoción. Se almacenan las promociones en un
 * HashMap y se asigna un valor entero a cada promoción para utilizarlo como identificador para seleccionar
 * o mostrar la promoción válida.
 *
 * El entero opcion permite identificar la promoción habilitada.
 */
public class Promociones
{

    private Map<Integer, IPromocion> promociones;
    private Integer opcion;

    public Promociones() {
        this.promociones = new HashMap<>();
        this.opcion = 1;
        this.promociones.put(1, new PromoCarnaval(5, 2023));
        this.promociones.put(2, new PromoHalloween(10, 2023));
        this.promociones.put(3, new PromoNavidad(15, 2023));

    }

    /**
     * Selecciona una promoción de la lista de promociones de acuerdo a un número ingresado. Si se ingresa un número
     * que no pertenezca a ninguna promoción se selecciona la promoción carnaval de forma predeterminada.
     */
    public void seleccionarPromo() {
        this.promociones.forEach((key, value) -> System.out.println(key + " " + "PROMOCION " + value.getNombre()));
        Scanner scanner = new Scanner(System.in);
        System.out.println("ingrese la promoción que quiere habilitar:");
        int input = scanner.nextInt(); // Opción para seleccionar una promoción
        this.opcion = this.promociones.containsKey(input) ? input : 1;
        System.out.println("promoción " + promociones.get(this.opcion).getNombre() + " habilitada!!!");
    }

    /**
     * Devuelve la descripción y características de la promoción válida y seleccionada.
     * @return cadena de texto con la descripción de la promoción.
     */
    public String verPromoValida() {
        IPromocion promo = this.promociones.get(this.opcion);
        return "--------------PROMOCIÓN " + promo.getNombre() + "-------------" + " \n" +
                "descuento= " + promo.getDescuento() + "%" + " \n" +
                "descripcion= " + promo.getDescripcion() + " \n" +
                "Fecha de inicio= " + promo.getStartDate() + " \n" +
                "Fecha de finalización= " + promo.getFinishDate();
    }

}