package Interfaces;

/**
 * Interfaz que permite implementar el comportamiento común en todos los tipos de motores.
 */
public interface IEngine
{
    /**
     * Enciende el motor a probar.
     */
    void iniciar();

    /**
     * Apaga el motor que se esta probando.
     */
    void apagar();

    /**
     * Permite aumentar la velocidad del motor en funcionamiento.
     */
    void acelerar();

    /**
     * Permite reducir la velocidad del motor en funcionamiento.
     */
    void frenar();

    /**
     * Permite rellenar el tanque de combustible de los motores.
     */
    void refuel();
}