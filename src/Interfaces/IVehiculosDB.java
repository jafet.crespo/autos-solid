package Interfaces;

/**
 * Interfaz que permite desplegar las funcionalidades de una lista de vehículos.
 */
public interface IVehiculosDB
{
    /**
     * Muestra todos los vehículos almacenados con su respectiva información.
     * Para mostrar cada vehículo se utiliza el formato JSON.
     */
    void mostrarVehiculos();

    /**
     * Elimina un elemento de la lista de vehículos almacenados.
     * Se busca el elemento a eliminar mediante su atributo Id.
     * @return cadena de texto de éxito al eliminar un vehículo o un mensaje de fracaso si se tiene un Id que es
     * incorrecto o no existe.
     */
    String eliminarPorId();
}