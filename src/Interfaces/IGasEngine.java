package Interfaces;

/**
 * Interfaz que permite implementar los métodos para cargar combustible para un motor
 * a gasolina.
 */
public interface IGasEngine extends IEngine
{
    /**
     * Permite llenar el tanque de combustible para motores a gasolina.
     */
    void fillGasTank();

    /**
     * Muestra el tamaño máximo del tanque de gasolina.
     */
    void maxSize();
}