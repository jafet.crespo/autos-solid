package Interfaces;

/**
 * Interfaz que permite implementar los métodos para recargar energía a los motores
 * eléctricos.
 */
public interface IElectricEngine extends IEngine
{
    /**
     * Recarga las celdas del motor eléctrico.
     */
    void chargeBattery();

    /**
     * Muestra la vida útil de la batería del auto eléctrico.
     */
    void baterryLife();
}