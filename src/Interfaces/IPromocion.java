package Interfaces;

import java.time.LocalDate;

/**
 * Interfaz que permite crear promociones para el proyecto. Se debe implementar en todas las clases
 * promociones. Las funcionalidades son declaradas en las clases que implementen la interfaz.
 */
public interface IPromocion
{
    /**
     * Obtiene el nombre de la promoción.
     * @return una cadena de texto con el nombre.
     */
    String getNombre();

    /**
     * Obtiene el descuento que se aplica en la promoción.
     * @return un número entero que representa el porcentaje del descuento.
     */
    int getDescuento();

    /**
     * Obtiene una pequeña descripción de la promoción.
     * @return cadena de texto que describe la promoción.
     */
    String getDescripcion();

    /**
     * Obtiene la fecha de inicio de la promoción.
     * @return mes y día que empieza la promoción.
     */
    LocalDate getStartDate();

    /**
     * Obtiene la fecha de finalización de la promoción.
     * @return mes y día que termina la promoción.
     */
    LocalDate getFinishDate();

}