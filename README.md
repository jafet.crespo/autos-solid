# PRACTICA SOLID Y GIT

## Introducción

El siguiente proyecto tiene la temática de una concesionaria de vehículos y que también realiza la 
importación de motores por pedido. Se pide un proyecto realizado con __*Java*__ y que cuente con
las siguientes funcionalidades: 

1. Mostrar una lista de los vehículos que se encuentran en stock.
2. Eliminar un vehículo de la lista por medio de su id.
3. Probar los motores que se encuentran en stock.
4. Recargar el combustible de los automóviles que se usaron para probar los motores.
5. Cambiar la promoción del mes.
6. Ver la promoción vigente actualmente.

Para su elaboración se debe manejar __*Git*__ como controlador de versiones y debe cumplir con
todos los principios __*SOLID*__ 

## S - Single responsibility

Cada clase debe ser responsable de una sola cosa. Un ejemplo de ello es la clase __*Vehiculo*__ y
la clase __*VehiculosDB*__: 

```java
 public abstract class Vehiculo {

    private int id;
    private String marca;
    private String color;

    public Vehiculo(int id, String marca, String color) {
        this.id = id;
        this.marca = marca;
        this.color = color;
    }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "id=" + this.id +
                ", marca='" + this.marca + '\'' +
                ", color='" + this.color + '\'' +
                ", caracteristicas= " + caracteristicaEspecial() +
                '}';
    }

    public abstract  String caracteristicaEspecial();
}
```

En la clase __*Vehiculo*__ se tiene la responsabilidad unica de crear un vehículo y sus
atributos. Por otro lado, en la clase __*VehiculosDB*__ la clase tiene los métodos de mostrar los
elementos de una lista de vehículos o eliminarlos.


```java

public class VehiculosDB implements IVehiculosDB {

    private ArrayList<Vehiculo> listaVehiculos ;


    public VehiculosDB() {
        this.listaVehiculos = new ArrayList<>();
        this.listaVehiculos.add(new Motocicleta(1, "moto 1", "rojo", "780MM"));
        this.listaVehiculos.add(new Motocicleta(2, "moto 2", "azul", "780MM"));
        this.listaVehiculos.add(new AutoLujo(3, "lujo 1", "amarrillo", "22 millones"));
        this.listaVehiculos.add(new AutoLujo(4, "lujo 1", "verde", "15 millones"));
        this.listaVehiculos.add(new Camioneta(5, "camioneta 1", "rojo", "2 TON"));
        this.listaVehiculos.add(new Camioneta(6, "camioneta 2", "verde", "1.5 TON"));
    }


    @Override
    public void mostrarVehiculos(){
        listaVehiculos.forEach(vehiculo -> System.out.println(vehiculo.toString()));
    }

    @Override
    public String eliminarPorId() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ingrese el id de un vehiculo:");
        int input = scanner.nextInt();
        return this.listaVehiculos.removeIf(vehiculo -> vehiculo.getId() == input) ? "vehiculo eliminado!!" : "Id incorrecto";
    }
}
```

Cada clase tiene tareas únicas, si se desea agregar un nuevo atributo a los vehículos solo se 
debe agregar estos a la clase __*Vehiculo*__, si se desea agregar funcionalidades se pueden 
agregar a la clase __*VehiculosDB*__.

De igual forma la clase __*Menu*__ tiene la responsabilidad de desplegar cada una de las 
opciones y funcionalidades del proyecto.

```java
public class Menu {

    private IVehiculosDB vehiculos;
    private MotoresService motores;
    private PromocionService promociones;

    public Menu() {
        this.vehiculos = new VehiculosDB();
        this.promociones = new PromocionService();
        this.motores = new MotoresService();
    }

    public void menuTabla() {
        System.out.println("----------OPCIONES---------");
        System.out.println("1.- Mostrar vehiculos");
        System.out.println("2.- Eliminar vehiculo por id");
        System.out.println("3.- Probar motores en stock");
        System.out.println("4.- Recargar combustible");
        System.out.println("5.- Cambiar promocion");
        System.out.println("6.- Ver promocion actual");
        System.out.println("0 salir del sistema");
        System.out.println("Ingrese una opción: ");
    }

    public void menuOpciones(int opcion) {
        switch (opcion) {
            case 1 -> this.vehiculos.mostrarVehiculos();
            case 2 -> System.out.println(this.vehiculos.eliminarPorId());
            case 3 -> this.motores.probarMotores();
            case 4 -> this.motores.recargarMotores();
            case 5 -> this.promociones.seleccionarPromo();
            case 6 -> System.out.println(this.promociones.verPromoValida());
            case 0 -> System.out.println("Hasta Pronto!!!");
        }
    }

}
```
## O - Open/closed

El principio nos dice que nuestras clases deben estar abiertas a extender y no a modificarse.
Un ejemplo de esto son las clases __*Vehiculo*__ y sus hijos. Cada hijo tiene características
especiales y son de un tipo diferente.

```java
//CLASE CAMIONETA

public class Camioneta extends Vehiculo {

    private String tipo;
    private String cargaMaxima;

    public Camioneta(int id, String marca, String color, String cargaMaxima) {
        super(id, marca, color);
        this.tipo = "camioneta";
        this.cargaMaxima = cargaMaxima;
    }

    @Override
    public String caracteristicaEspecial() {
        return "{" +
                "tipo='" + this.tipo + '\'' +
                ", cargaMaxima='" + this.cargaMaxima + '\'' +
                '}';
    }
}


// CLASE MOTOCICLETA

public class Motocicleta extends Vehiculo{

    private String tipo;
    private String seatHeight;

    public Motocicleta(int id, String marca, String color, String seatHeight) {
        super(id, marca, color);
        this.tipo = "motocicleta";
        this.seatHeight = seatHeight;
    }

    @Override
    public String caracteristicaEspecial() {
        return "{" +
                "tipo='" + this.tipo + '\'' +
                ", seatHeight='" + this.seatHeight + '\'' +
                '}';
    }

}
```
Se tiene los siguientes métodos para mostrar cada vehículo de forma individual o eliminarlos
de una lista:

```renderscript

//CODIGO EXTRAIDO DE LA CLASE VehiculosDB

 void mostrarVehiculos(){
        listaVehiculos.forEach(vehiculo -> System.out.println(vehiculo.toString()));
}

String eliminarPorId() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ingrese el id de un vehiculo:");
        int input = scanner.nextInt();
        return this.listaVehiculos.removeIf(vehiculo -> vehiculo.getId() == input) 
        ? "vehiculo eliminado!!" 
        : "Id incorrecto";
}
```

En ambas funciones si se quiere agregar otro tipo de vehículo, no es necesario modificar el
comportamiento de las funciones. Solo se debe agregar otra clase que extienda de la clase
__*Vehiculo*__ y darle los atributos que uno vea conveniente. De esta forma se cumple con 
el principio open/closed, ya que no se requiere modificaciones, sino extender una clase.

Otros ejemplos de este principio aplicados al proyecto son las clases relacionadas a los 
motores y promociones. Ambas pueden extender a partir de implementar las interfaces 
__*IEngine*__ y __*IPromocion*__ respectivamente, sin necesidad de modificar código en las
funcionalidades que involucran el uso de estas clases.


# L - Linskov substitution

El principio de sustitución de Linkskov nos dice que las clases hijas pueden tomar el lugar
de las clases padre sin ningún problema. Este principio se observa al ver como las
clases hijas de __*Vehiculo*__ pueden remplazar al padre sin causar ningún problema.

Sin embargo, otra forma de ver la aplicación de dicho principio en el proyecto es por medio
de las clases de motores, las cuales implementan la interfaz __*IEngine*__:

```java
public interface IEngine {
    void iniciar();
    void apagar();
    void acelerar();
    void frenar();
    void refuel();
}
```
La interfaz motor, obliga a implementar las funciones para probar un motor, sin embargo,
cada tipo de motor tiene una forma diferente de hacerlo, pero cumplen con las funcionalidades
del padre.

```java

//CLASE MOTOR FORD A GASOLINA

public class FordEngine implements IGasEngine {

    @Override
    public void iniciar() {
        System.out.println("INICIANDO motor a gasolina Ford........");
    }

    @Override
    public void apagar() {
        System.out.println("APAGANDO motor a gasolina Ford...... ");
    }

    @Override
    public void acelerar() {
        System.out.println("ACELERANDO motor a gasolina Ford...... ");
    }

    @Override
    public void frenar() {
        System.out.println("FRENANDO motor a gasolina Ford...... ");
    }

    @Override
    public void refuel() {
        fillGasTank();
        maxSize();
    }

    @Override
    public void fillGasTank() {
        System.out.println("Llenando Tanque de gasolina...... ");
    }

    @Override
    public void maxSize() {
        System.out.println("Maximo 17 galones ");
    }
}

//CLASE MOTOR TESLA ELECTRICO

public class TeslaEngine implements IElectricEngine {

    @Override
    public void chargeBattery() {
        System.out.println("CARGANDO BATERIA.......");
    }

    @Override
    public void baterryLife() {
        System.out.println("tiempo de vida 10 años");
    }

    @Override
    public void iniciar() {
        System.out.println("INICIANDO motor electrico Tesla........");
    }

    @Override
    public void apagar() {
        System.out.println("APAGANDO motor electrico Tesla...... ");
    }

    @Override
    public void acelerar() {
        System.out.println("ACELERANDO motor electrico Tesla...... ");
    }

    @Override
    public void frenar() {
        System.out.println("FRENANDO motor electrico Tesla...... ");
    }

    @Override
    public void refuel() {
        chargeBattery();
        baterryLife();
    }
}
```

Y al momento de implementar y probar motores de diferentes tipos de una lista podemos 
ejecutar la siguiente función sin ningún problema:

```renderscript
// CODIGO EXTRAIDO DE LA CLASE MotoresService

public void probarMotores(){
    this.listaMotores.forEach(motor -> {
    motor.iniciar();
    motor.acelerar();
    motor.frenar();
    motor.apagar();
    System.out.println("---------------------------------------");
});
}
```

## I - Interface segregation

El principio nos dice que no se debe forzar a implementar funciones que no se utilizan de
una interfaz. Para este principio se implemento dos interfaces derivadas de la interfaz
__*IEngine*__:

```java
public interface IGasEngine extends IEngine {
    void fillGasTank();
    void maxSize();
}

public interface IElectricEngine extends IEngine{
    void chargeBattery();
    void baterryLife();
}
```

Los motores recargan combustible, sin embargo, depende del tipo pueden hacerlo de diferentes
maneras. En el caso de los motores a gasolina se puede rellenar tanque con gasolina y los 
motores eléctricos recargan la batería y tienen un tiempo de vida. En el codigo de la
clase __*ToyotaEngine*__ se puede ver una clase que implementa la interfaz __*IGasEngine*__:
```java
public class ToyotaEngine implements IGasEngine {
    
    @Override
    public void refuel() {
        fillGasTank();
        maxSize();
    }

    @Override
    public void fillGasTank() {
        System.out.println("Llenando Tanque de gasolina...... ");
    }

    @Override
    public void maxSize() {
        System.out.println("Maximo 19 galones ");
    }
}
```
Por tanto, se puede hacer uso de la siguiente funcionalidad sin tener problemas gracias a
segregar las interfaces:
```renderscript
public void recargarMotores() {
    this.listaMotores.forEach(motor -> {
    motor.refuel();
    System.out.println("---------------------------------------");
});
}
```

## D - Dependency inversion

El principio nos dice que los módulos de alto nivel no deben depender de módulos de bajo nivel,
ambos deben depender de la abstracción. Para este caso se tiene el ejemplo las clases 
promociones, estas clases implementan la interfaz __*IPromocion*__:

```java
public interface IPromocion {

    String getNombre();
    int getDescuento();
    String getDescripcion();

    LocalDate getStartDate();
    LocalDate getFinishDate();

}
```

A partir de dicha interfaz se tiene diferentes promociones como la siguiente:
```java
public class PromoNavidad implements IPromocion {

    private String nombre;
    private int descuento;
    private String descripcion;
    private LocalDate startDate;
    private LocalDate finishDate;

    public PromoNavidad(int descuento, int year) {
        this.descuento = descuento;
        this.nombre = "NAVIDAD";
        this.descripcion = "Promoción por fechas navideñas, traer un juguete para aplicar el descuento";
        this.startDate = LocalDate.of(year, 12,1);
        this.finishDate = LocalDate.of(year,12, 31);
    }

    public String getNombre() {
        return this.nombre;
    }
    @Override
    public int getDescuento() {
        return this.descuento;
    }

    @Override
    public String getDescripcion() {
        return this.descripcion;
    }

    @Override
    public LocalDate getStartDate() {
        return this.startDate;
    }

    @Override
    public LocalDate getFinishDate() {
        return this.finishDate;
    }
}
```
Para ver como el proyecto cumple el principio de inversión de dependencias se tiene la
siguiente funcionalidad:

```renderscript
public String verPromoValida() {
    IPromocion promo = this.promociones.get(this.opcion);
    return "--------------PROMOCIÓN " + promo.getNombre() + "-------------" + " \n"  +
            "descuento= " + promo.getDescuento() + "%" + " \n"   +
            "descripcion= " + promo.getDescripcion() + " \n" +
            "Fecha de inicio= " + promo.getStartDate() + " \n"  +
            "Fecha de finalización= " + promo.getFinishDate();
}
```
El momento en que se cambien la promoción no se tiene la necesidad de modificar o cambiar
dicha función, y si se quiere añadir una nueva promoción es tan facil como crear una clase
que implemente la interfaz __*IPromocion*__ y añadirla a la lista de promociones.


Otra forma de ver como el proyecto cumple con este principio es la interfaz __*IvehiculosDB*__:

```java
public interface IVehiculosDB {

    void mostrarVehiculos();
    String eliminarPorId();
}
```

Dicha interfaz es implementada en la clase __*VehiculosDB*__ la cual permite almacenar los
datos de los vehículos por medio de un ArrayList. Sin embargo, si el proyecto lo requiere
podemos utilizar una base de datos o otro metodo de almacenamiento. Para hacer este cambio
solo debemos crear una clase que implemente la interfaz __*IvehiculosDB*__, y no se requiere
modificar ninguna clase.
```renderscript
// PARTE DE LA CLASE Menu

private IVehiculosDB vehiculos;

public Menu(IVehiculosDB vehiculos) {
    this.vehiculos = vehiculos;

}
```
Como se puede observar las funcionalidades del menu dependen de la interfaz no de la clase
específica.

## Conclusión

Para que un proyecto cumpla con los principios SOLID se debe tener un conocimiento claro de
los pilares de la programación orientada a objetos. Se debe tener un buen manejo y entendimiento 
de lo que son las clases abstractas e interfaces. Otro aspecto que ayuda a la aplicación de los
principios SOLID es tener las clases ordenadas y mantener un código limpio.